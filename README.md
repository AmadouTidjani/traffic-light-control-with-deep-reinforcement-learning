# Traffic Light Control with Deep Reinforcement Learning

## Project's Description
Cette méthode innovante de modélisation propose une nouvelle façon de gérer intelligemment les systèmes de transport en contrôlant efficacement les feux de circulation. L'objectif est ainsi de minimiser les temps d'attente et les taux d'arrêt des véhicules aux intersections, ce qui est formulé comme une fonction de récompense négative basée sur ces deux critères. En d'autres termes, l'agent cherche à maximiser une récompense négative qui est inversement proportionnelle à la somme des temps d'attente et des taux d'arrêt des véhicules.

### Single intersection
A l'état de l'art, plusieurs implémentations d'environnement du trafic routier sont possibles. Pour simplifier notre travail d'ordre académique, nous considérons une simple intersection avec seulement deux routes unidimensionnelles, sans virages à gauche ou à droite. Ainsi, nous implémentons un modèle d'apprentissage par renforcement qui cherchera à optimiser la circulation au niveau de cette intersection en contrôlant efficacement le feu de circulation.

## Markov Decision Process
Nous modélisons le problème comme un processus de décision markovien en définissant les états, les actions et la récompense. 
Considérons le nombre de voitures sur chaque route à l'instant $t$ désigné par $Q_1(t), Q_2(t) \in \mathbb{I}$ respectivement. On a la relation suivante :
$$Q_i(t+1) = Q_i(t) + C_i(t) - D_i(t)$$
$i\in \{1, 2\}$, les variables $C_i(t)$ et $D_i(t)$ sont respectivement les voitures entrants et sortants dans la voie $i$.

Notons que toutes ces variables bien que discrétisées sont aléatoires.
### Agent 
L'agent est ici le système de contrôle de trafic routier. Il doit prendre la décision sur le moment par le feu à rouge ou vert sur chacune de deux routes. Soit $A(t)$ l'action prise à l'instant, elle est dans $\{0, 1\}$ avec 0 (changer) et 1 (continuer).

### Les états 
Nous désigons l'état du feu de circulation par $S$, qui peut être l'un des quatre états suivants.

* "0": feu vert pour la route $Q_1$, et donc feu rouge pour la route $Q_2$;
* "1": feu rouge pour la route $Q_1$, et donc feu vert pour la route $Q_2$;
* "2": feu jaune pour $Q_1$, et feu rouge pour la route $Q_2$;
* "3": feu rouge pour la route $Q_1$, et feu jaune pour la route $Q_2$;

### Action
L'action ou la transition d'états va être binaire :


* "0": changer;
* "1": continuer;


Selon les principes généraux de transport, la transition d'état des feux de circulation ne peut être que dans une direction, qui est "0" -> "2" -> "1" -> "3" -> "0" selon notre définition des états des feux ci-dessus. L'agent RL entraîné prend le tuple $[Q_1, Q_2, S]$ en entrée et génère un choix d'action pour le feu de circulation.

Ainis, l'état du feu à l'instant $t+1$ est défini comme suit :

$$Q_i(t+1) \equiv Q_i(t) + A_i(t) [4]$$

<p align="center">
<img src="./save/1inter.png" width="25%">
</p>

### Rewards
Comme évoqué plus haut, nous définisssons la récompense $R = -(Q_1^2+Q_2^2)$

On défini alors le coût de l'embouteillage $V_{\pi}$ sur une période en discrétisant le temps :
$$V_{\pi} = -\frac{1}{T}\mathbf{E}[\sum_{t=0}^T \gamma^tR(t)]$$ 

Par là, nous pouvons montrer que le coût sous la politique optimale vérifiie :
$$V_{\hat{\pi}} = \max\{R(t) + \gamma \max\sum_{k=1}^{T-t}\gamma^{k-1}R(t+k)\}$$

Ainsi, on a la recompense (Q-value) sous la politique optimale qui satisfait la relation suivante :
$$Q(s, a) = r(s,a) + \gamma\sum_{s'\in S}p(s, s'; a)\underset{a^{'}\in\mathcal{A}}{\max}Q(s', a')$$

$$Q(s, a) = r(s,a) + \gamma\mathbf{E}[\underset{a'\in\mathcal{A}}{\max}Q(s', a')]$$

Où $r(s,a)$ est la recompense à l'état $s = (Q_1, Q_2, L)$ et $p$ la probabilité de transition entre les états $s$ et $s'$

Pour les détails sur les démonstrations, vous pouvez consulter l'article cité dans les références.
## Project's content

Le projet contient principalement :

* fichier `deepqnet.py` qui implémente l'algorithme du Deep Q learning utilisant la librairie tensorflow.
* fichier `inference.py` qui comporte le code d'entrainement de la solution RL
* dossier `save` qui enregistre les résultats graphiques du modèle et qui comporte le `.png` de l'environnement modélisé
* dossier `requirements.txt` pour les librairies python à installer

## Utilisation du code
Pour utiliser ce code, veuillez cloner le repository. Après, vous exécutez les commandes suivantes. La première python est pour l'entraineement du réseau et la deuxième pour le test.

+ `pip install -r requirements.txt`
+ `python inference.py --train`
+ `python inference.py --test`


## Results 

### Train

##### Rewards value
<p align="center">
<img src="./save/train.png" width="40%">
</p>

##### Loss by step
<p align="center">
<img src="./save/cost.png" width="40%">
</p>


### Test
<p align="center">
<img src="./save/test.png" width="40%">
</p>

## Références

- Cours apprentissage par renforcement de Rémi Besson et Frédéric Logé, ENSAI 2022-2023
- Liu and al., Deep Reinforcement Learning for Trafﬁc Light
Control in Intelligent Transportation Systems